import { useRouter } from "next/navigation";
import { withAuthGuard } from "../hocs/with-auth-guard";
const Page = () => {
  const router = useRouter();
  router.replace("/dashboard");
  return <></>;
};

export default withAuthGuard(Page);
